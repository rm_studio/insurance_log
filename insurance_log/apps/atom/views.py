# encoding: utf8

from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.template import loader, Context
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth import authenticate, login, logout
from django.utils.translation import ugettext_lazy as _
from insurance_log.apps.ilmember.models import *
from insurance_log.apps.company.models import *
from insurance_log.apps.progress.models import *

from django.utils import timezone


def index(request):
    t = loader.get_template("index.html")
    c = Context()
    return render_to_response('index.html', {}, context_instance=RequestContext(request))


def detail(request, offset):
    return render_to_response('detail.html',
        { 'p': Progress.objects.get(pk=int(offset)), },
        context_instance=RequestContext(request))


def create(request):
    if request.POST.get('license_plate') and request.POST.get('enter_time'):
        p = Progress( 
            enter_time=request.POST.get('enter_time'),
            company=request.user.company,
            manager=IlMember.objects.get(pk=int(request.POST.get('manager'))),
            license_plate=request.POST.get('license_plate'),
            steps=1
        )
        p.save()
        return HttpResponseRedirect('/progress/%d?step=%d' % (int(p.pk), 1))
    
    else: 
        return render_to_response('steps/new.html', 
            { 'manager': IlMember.get_active_company_managers(request.user.company) },
            context_instance=RequestContext(request))


def step(request, offset):
    '''
    {
        1: lambda: p = Progress()
        2: lambda:
    }[offset]()
    '''


    if request.GET.get('step'):
        ref = lambda x: HttpResponseRedirect('?step=%d' % (int(x)))
        is_staff = lambda : request.user not in IlMember.get_active_company_administrator(request.user.company) and True or False
        print is_staff()

        p = Progress.objects.get(pk=int(offset))
        step = int(request.GET.get('step'))

        if p.steps != step and is_staff(): return ref(p.steps)
       
        if request.POST.get('csrfmiddlewaretoken'):
            try: 
                if step==1:
                    p.crack_type = request.POST.get('crack_type') 
                    p.crack_type_2 = request.POST.get('crack_type_2') 
                    p.crack_type_time = timezone.now()
                elif step==2:
                    p.vehicle_license = GalleryUser.objects.get(name='%d-%d' % (p.pk,step))
                    p.vehicle_license_time = timezone.now()
                    if p.crack_type_2 == '三者车':
                        step = 6
                elif step==3:
                    p.driving_license = GalleryUser.objects.get(name='%d-%d' % (p.pk,step))
                    p.driving_license_time = timezone.now()
                elif step==4:
                    try:
                        p.insurance_card = GalleryUser.objects.get(name='%d-%d' % (p.pk,step))
                    except:
                        pass
                    p.insurance_card_number = request.POST.get('insurance_card_number')
                    p.insurance_card_time = timezone.now()
                elif step==5:
                    p.id_card = GalleryUser.objects.get(name='%d-%d' % (p.pk,step))
                    p.id_card_number = request.POST.get('id_card_number')
                    p.id_card_time = timezone.now()
                elif step==6:
                    p.credit_card = GalleryUser.objects.get(name='%d-%d' % (p.pk,step))
                    p.credit_card_time = timezone.now()
                elif step==7:
                    p.insurance_company = Company.objects.get(pk=int(request.POST.get('insurance_company')))
                    p.insurance_company_time = timezone.now()
                elif step==8:
                    p.surface_confirm_manager = IlMember.objects.get(pk=int(request.POST.get('surface_confirm_manager'))) 
                    p.surface_confirm_time = request.POST.get('surface_confirm_time')
                elif step==9:
                    p.inside_confirm_manager = IlMember.objects.get(pk=int(request.POST.get('inside_confirm_manager'))) 
                    p.inside_confirm_time = request.POST.get('inside_confirm_time')
                    if p.crack_type == '单方(免票)':
                        step = 10
                elif step==10:
                    p.confirm = GalleryUser.objects.get(name='%d-%d' % (p.pk,step))
                    p.confirm_time = timezone.now()
                elif step==11:
                    p.damage_value = request.POST.get('damage_value')
                    p.damage_value_time = timezone.now()
                    if p.crack_type == '单方(免票)':
                        step = 17
                elif step==12:
                    p.hand_in_case_type = request.POST.get('hand_in_case_type')
                    p.hand_in_case_type_time = timezone.now()
                    if p.hand_in_case_type == '快赔':
                        step = 17
                elif step==13:
                    p.damage_doc = GalleryUser.objects.get(name='%d-%d' % (p.pk,step)) 
                    p.damage_doc_time = timezone.now()
                elif step==14:
                    p.old_item_recycle_time = request.POST.get('old_item_recycle_time') 
                elif step==15:
                    try:
                        p.receipt = GalleryUser.objects.get(name='%d-%d' % (p.pk,step)) 
                    except:
                        pass
                    p.receipt_time = timezone.now()
                elif step==16:
                    p.car_check_time = request.POST.get('car_check_time')
                elif step==17:
                    if request.POST.get('case_can_close') == 'yes':
                        p.case_can_close = True
                    else:
                        p.case_can_close = False
                        p.case_close = timezone.now()
                        step = 18
                    p.comment = request.POST.get('comment')
                elif step==18:
                    p.case_close = request.POST.get('case_close') 

                '''
                print [p,request.POST]    
                for name,value in vars(Progress).items():    
                    for pn,pv in request.POST.items():
                        if pn.encode("ascii") == name:
                            print('%s=%s'%(name,value))
                '''

                p.warning_state = False
                if is_staff(): p.steps = int(step) + 1
                p.save() 
                
                return ref(step+1)

            except:
                return ref(step)

        if step>=19:
            return HttpResponseRedirect('/detail/%d' % int(p.pk) )  
        else:
            return render_to_response('steps/%d.html' % step, 
                    {"p": p, "step": step, "companies": Company.get_insurance_companies(request.user.company),
                     "members": IlMember.get_active_company_managers(request.user.company), }, 
                    context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect('/')


def user_login(request):

    if request.method == 'POST':
        if not request.POST.get('account'):
            error = (_('please_input_account'))
            return render_to_response('login.html', {"error": error}, context_instance=RequestContext(request))
        if not request.POST.get('password'):
            error = (_('please_input_password'))
            return render_to_response('login.html', {"error": error}, context_instance=RequestContext(request))
        account = request.POST.get('account')
        password = request.POST.get('password')
        user = authenticate(username=account, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/check_warning')
            else:
                error = (_('disabled_account'))
                return render_to_response('login.html', {"error": error}, context_instance=RequestContext(request))
        else:
            error = (_('login_error'))
            return render_to_response('login.html', {"error": error}, context_instance=RequestContext(request))

    t = loader.get_template("login.html")
    c = Context()
    return render_to_response('login.html', {}, context_instance=RequestContext(request))


def user_logout(request):
    logout(request)
    error = (_('thanks for your time'))
    return render_to_response('login.html', {"error": error}, context_instance=RequestContext(request))
