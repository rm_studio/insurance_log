from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from insurance_log.apps.progress.models import Progress, WarningSetting
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.decorators import login_required
import json


@login_required
def all_progresses(request):

    progress = Progress()
    progresses = progress.get_progresses(request.user.company)

    warning = WarningSetting()
    warning = warning.get_warning_setting(request.user.company)

    return render_to_response('index.html', {"progresses": progresses, "warning": warning}, context_instance=RequestContext(request))


def warning_progresses(request):

    progress = Progress()
    progresses = progress.get_warning_progresses(request.user.company)

    warning = WarningSetting()
    warning = warning.get_warning_setting(request.user.company)

    return render_to_response('index.html', {"progresses": progresses, "warning": warning}, context_instance=RequestContext(request))


def ongoing_progresses(request):

    progress = Progress()
    progresses = progress.get_ongoing_progresses(request.user.company)

    warning = WarningSetting()
    warning = warning.get_warning_setting(request.user.company)

    return render_to_response('index.html', {"progresses": progresses, "warning": warning}, context_instance=RequestContext(request))


def search_progresses(request):

    key = request.GET.get('key')

    progress = Progress()
    progresses = progress.search_progresses(request.user.company, key)

    warning = WarningSetting()
    warning = warning.get_warning_setting(request.user.company)

    return render_to_response('index.html', {"progresses": progresses, "warning": warning}, context_instance=RequestContext(request))


def progress_detail(request, offset):

    progress = Progress()
    progress = progress.get_progress(offset)

    warning = WarningSetting()
    warning = warning.get_warning_setting(request.user.company)

    return render_to_response('detail.html', {"progress": progress, "warning": warning}, context_instance=RequestContext(request))


def warning_setting_detail(request):

    warning_setting = WarningSetting()
    t_warning_setting = warning_setting.get_warning_setting(request.user.company)

    return render_to_response('warning_setting.html', {"warning": t_warning_setting}, context_instance=RequestContext(request))


def edit_warning_setting(request):
    error = None
    warning = WarningSetting()
    t_warning = warning.get_warning_setting(request.user.company)
    print("t_warning:")
    print(t_warning)

    if request.POST.get('crack_type_warning') != "":
        print(".....")
        t_warning.crack_type_warning = request.POST.get('crack_type_warning')
    if request.POST.get('vehicle_license_warning') != "":
        t_warning.vehicle_license_warning = request.POST.get('vehicle_license_warning')
    if request.POST.get('driving_license_warning') != "":
        t_warning.driving_license_warning = request.POST.get('driving_license_warning')
    if request.POST.get('id_card_warning') != "":
        t_warning.id_card_warning = request.POST.get('id_card_warning')
    if request.POST.get('insurance_card_warning') != "":
        t_warning.insurance_card_warning = request.POST.get('insurance_card_warning')
    if request.POST.get('credit_card_warning') != "":
        t_warning.credit_card_warning = request.POST.get('credit_card_warning')
    if request.POST.get('surface_confirm_warning') != "":
        t_warning.surface_confirm_warning = request.POST.get('surface_confirm_warning')
    if request.POST.get('inside_confirm_warning') != "":
        t_warning.inside_confirm_warning = request.POST.get('inside_confirm_warning')
    if request.POST.get('confirm_warning') != "":
        t_warning.confirm_warning = request.POST.get('confirm_warning')
    if request.POST.get('damage_value_warning') != "":
        t_warning.damage_value_warning = request.POST.get('damage_value_warning')
    if request.POST.get('hand_in_case_warning') != "":
        t_warning.hand_in_case_warning = request.POST.get('hand_in_case_warning')
    if request.POST.get('damage_doc_warning') != "":
        t_warning.damage_doc_warning = request.POST.get('damage_doc_warning')
    if request.POST.get('old_item_recycle_warning') != "":
        t_warning.old_item_recycle_warning = request.POST.get('old_item_recycle_warning')
    if request.POST.get('receipt_warning') != "":
        t_warning.receipt_warning = request.POST.get('receipt_warning')
    if request.POST.get('car_check_warning') != "":
        t_warning.car_check_warning = request.POST.get('car_check_warning')
    if request.POST.get('damage_doc_warning') != "":
        t_warning.case_close_warning = request.POST.get('case_close_warning')

    try:
        t_warning.save()
    except:
        error = (_('warning setting save fail'))

    t_warning = warning.get_warning_setting(request.user.company)

    return render_to_response('warning_setting.html', {"warning": t_warning, "error": error}, context_instance=RequestContext(request))


def check_warning(request):

    progress = Progress()
    progresses = progress.get_ongoing_progresses(request.user.company)

    warning = WarningSetting()
    t_warning = warning.get_warning_setting(request.user.company)

    for progress in progresses:
        if check_undone(progress, progress.case_close, progress.car_check_time, t_warning.case_close_warning):
            continue
        if check_undone(progress, progress.car_check_time, progress.receipt_time, t_warning.car_check_warning):
            continue
        if check_undone(progress, progress.receipt_time, progress.old_item_recycle_time, t_warning.receipt_warning):
            continue
        if check_undone(progress, progress.old_item_recycle_time, progress.damage_doc_time, t_warning.old_item_recycle_warning):
            continue
        if check_undone(progress, progress.damage_doc_time, progress.hand_in_case_type_time, t_warning.damage_doc_warning):
            continue
        if check_undone(progress, progress.hand_in_case_type_time, progress.damage_value_time, t_warning.hand_in_case_warning):
            continue
        if check_undone(progress, progress.damage_value_time, progress.confirm_time, t_warning.damage_value_warning):
            continue
        if check_undone(progress, progress.confirm_time, progress.inside_confirm_time, t_warning.confirm_warning):
            continue
        if check_undone(progress, progress.inside_confirm_time, progress.surface_confirm_time, t_warning.inside_confirm_warning):
            continue
        if check_undone(progress, progress.surface_confirm_time, progress.insurance_company_time, t_warning.surface_confirm_warning):
            continue
        if check_undone(progress, progress.insurance_company_time, progress.credit_card_time, t_warning.insurance_company_warning):
            continue
        if check_undone(progress, progress.credit_card_time, progress.id_card_time, t_warning.credit_card_warning):
            continue
        if check_undone(progress, progress.id_card_time, progress.insurance_card_time, t_warning.id_card_warning):
            continue
        if check_undone(progress, progress.insurance_card_time, progress.driving_license_time, t_warning.insurance_card_warning):
            continue
        if check_undone(progress, progress.driving_license_time, progress.vehicle_license_time, t_warning.driving_license_warning):
            continue
        if check_undone(progress, progress.vehicle_license_time, progress.crack_type_time, t_warning.vehicle_license_warning):
            continue
        if check_undone(progress, progress.crack_type_time, progress.enter_time, t_warning.crack_type_warning):
            continue

    request.session['warning_count'] = warning_count(request.user.company)
    request.session['ongoing_count'] = ongoing_count(request.user.company)

    return HttpResponseRedirect('/')


def check_undone(progress, job_time, last_job_time, warning):
    print("this_job:"+str(job_time)+", last_job:"+str(last_job_time)+", warning:"+str(warning))
    if job_time is None and last_job_time is not None:
        print("last_job_time")
        if warning_state(last_job_time, warning):
            print("warning_state")
            progress.warning_state = True
            progress.save()
            print(progress.license_plate)
        return True
    return False


def warning_state(time, warning):
    i = timezone.now() - time
    if i.total_seconds() >= int(warning*60):
        return True
    return False


def warning_count(company):
    progress = Progress()
    progresses = progress.get_warning_progresses(company)
    return progresses.count()


def ongoing_count(company):
    progress = Progress()
    progresses = progress.get_ongoing_progresses(company)
    return progresses.count()


def get_warning_progresses_ajax(request):
    pg = Progress().get_warning_progresses(request.user.company)
    return HttpResponse( 
            json.dumps( { 
                'pk': [ p.pk for p in pg.all() ], 
                'steps': [ p.steps for p in pg.all() ],
                'percent': [ p.steps*100/19 for p in pg.all() ],
                'license_plate': [ p.license_plate for p in pg.all() ], 
            } ), 
            content_type="application/json")


@login_required
def delete_progress(request):
    if request.user.priority == 1:
        progress_id = request.GET.get('pk')
        progress = get_object_or_404(Progress, pk=progress_id)
        progress.delete()

        return HttpResponse(progress_id)
    else:
        from django.http import HttpResponseForbidden
        return HttpResponseForbidden()

