from django.contrib import admin
from .models import Progress, WarningSetting


class ProgressAdmin(admin.ModelAdmin):  
    list_display = ('id', 'license_plate', 'enter_time', 'manager', 'company', 'case_close', 'warning_state')
    search_fields = ('id', 'license_plate', 'warning_state')


class WarningSettingAdmin(admin.ModelAdmin):
    list_display = ('id', 'company')

# Register your models here.
admin.site.register(Progress, ProgressAdmin)
admin.site.register(WarningSetting, WarningSettingAdmin)

