from django.db import models
from django.utils.translation import ugettext_lazy as _
from insurance_log.apps.ilmember.models import IlMember
from insurance_log.apps.company.models import Company
from insurance_log.core.gallery.models import GalleryUser
from ckeditor.fields import RichTextField
from django.db.models import Q


class Progress(models.Model):
    license_plate = models.CharField(_('license_plate'), max_length=20)
    enter_time = models.DateTimeField(_('enter_time'), null=True, blank=True)
    manager = models.ForeignKey(IlMember, related_name="manager", default='', null=True, blank=True)

    crack_type = models.CharField(_('crack_type'), max_length=50, null=True, blank=True)
    crack_type_2 = models.CharField(_('crack_type_2'), max_length=50, null=True, blank=True)
    crack_type_time = models.DateTimeField(_('crack_type_time'), null=True, blank=True)

    vehicle_license = models.ForeignKey(GalleryUser, related_name='vehicle_license', null=True, blank=True)
    vehicle_license_time = models.DateTimeField(_('vehicle_license_time'), null=True, blank=True)
    driving_license = models.ForeignKey(GalleryUser, related_name='driving_license', null=True, blank=True)
    driving_license_time = models.DateTimeField(_('driving_license_time'), null=True, blank=True)
    insurance_card_number = models.CharField(_('insurance_card_number'), max_length=50, null=True, blank=True)
    insurance_card = models.ForeignKey(GalleryUser, related_name='insurance_card', null=True, blank=True)
    insurance_card_time = models.DateTimeField(_('insurance_card_time'), null=True, blank=True)
    id_card_number = models.CharField(_('id_card_number'), max_length=50, null=True, blank=True)
    id_card = models.ForeignKey(GalleryUser, related_name='id_card', null=True, blank=True)
    id_card_time = models.DateTimeField(_('id_card_time'), null=True, blank=True)
    credit_card = models.ForeignKey(GalleryUser, related_name='credit_card', null=True, blank=True)
    credit_card_time = models.DateTimeField(_('credit_card_time'), null=True, blank=True)

    insurance_company = models.ForeignKey(Company, related_name='insurance_company', null=True, blank=True)
    insurance_company_time = models.DateTimeField(_('insurance_company_time'), null=True, blank=True)

    surface_confirm_time = models.DateTimeField(_('surface_confirm_time'), null=True, blank=True)
    surface_confirm_manager = models.ForeignKey(IlMember, related_name='surface_confirm_manager', null=True, blank=True)
    inside_confirm_time = models.DateTimeField(_('inside_confirm_time'), null=True, blank=True)
    inside_confirm_manager = models.ForeignKey(IlMember, related_name='inside_confirm_manager', null=True, blank=True)

    confirm = models.ForeignKey(GalleryUser, related_name='confirm', null=True, blank=True)
    confirm_time = models.DateTimeField(_('confirm_time'), null=True, blank=True)

    damage_value = models.CharField(_('damage_value'), max_length=50, null=True, blank=True)
    damage_value_time = models.DateTimeField(_('damage_value_time'), null=True, blank=True)

    hand_in_case_type = models.CharField(_('hand_in_case_type'), max_length=50, null=True, blank=True)
    hand_in_case_type_time = models.DateTimeField(_('hand_in_case_type_time'), null=True, blank=True)
    damage_doc = models.ForeignKey(GalleryUser, related_name='damage_doc', null=True, blank=True)
    damage_doc_time = models.DateTimeField(_('damage_doc_time'), null=True, blank=True)
    old_item_recycle = models.BooleanField(_('old_item_recycle'), default=False, blank=True)
    old_item_recycle_time = models.DateTimeField(_('old_item_recycle_time'), null=True, blank=True)
    receipt = models.ForeignKey(GalleryUser, related_name='receipt',  null=True, blank=True)
    receipt_time = models.DateTimeField(_('receipt_time'), null=True, blank=True)
    car_check = models.BooleanField(_('car_check'), default=False, blank=True)
    car_check_time = models.DateTimeField(_('car_check_time'), null=True, blank=True)

    case_can_close = models.BooleanField(_('case_can_close'), default=False, blank=True)
    case_close = models.DateTimeField(_('case_close'), null=True, blank=True)

    company = models.ForeignKey(Company, null=True, blank=True)

    comment = RichTextField(_('comment'), default='',  null=True, blank=True)
    
    steps = models.IntegerField(_('Steps'), null=True, default=0, blank=True)

    warning_state = models.BooleanField(_('warning_state'), default=False, blank=True)

    class Meta:
        verbose_name = _('Progress')
        verbose_name_plural = _('Progresses')

    def list_all_member(self):
        for name,value in vars(self).items():
            print('%s=%s'%(name,value))

    def get_progresses(self, company):
        progresses = Progress.objects.filter(company=company).order_by("case_close", "-enter_time")
        return progresses

    def get_warning_progresses(self, company):
        progresses = Progress.objects.filter(Q(company=company) & Q(warning_state=True)).order_by("-enter_time")
        return progresses

    def get_ongoing_progresses(self, company):
        progresses = Progress.objects.filter(Q(company=company) & Q(case_close=None)).order_by("-enter_time")
        return progresses

    def get_progress(self, progress_id):
        progress = Progress.objects.get(id=progress_id)
        return progress

    def search_progresses(self, company, key):
        progresses = Progress.objects.filter(Q(company=company) & (Q(license_plate__contains=key) | Q(insurance_card_number__contains=key) | Q(id_card_number__contains=key))).order_by("-enter_time")
        return progresses

    def delete(self):
        print 'waiting for delete', self.pk
        galleries = GalleryUser.objects.filter(name__startswith='%d-'%self.pk)
        for gallery in galleries:
            for pic in gallery.galleryPic.all():
                pic.delete()
            gallery.delete()

        super(Progress, self).delete()


class WarningSetting(models.Model):
    company = models.ForeignKey(Company, null=True)
    crack_type_warning = models.IntegerField(_('crack_type_warning'), max_length=20, null=True)
    vehicle_license_warning = models.IntegerField(_('vehicle_license_warning'), max_length=20, null=True)
    driving_license_warning = models.IntegerField(_('driving_license_warning'), max_length=20, null=True)
    id_card_warning = models.IntegerField(_('id_card_warning'), max_length=20, null=True)
    insurance_card_warning = models.IntegerField(_('insurance_card_warning'), max_length=20, null=True)
    credit_card_warning = models.IntegerField(_('credit_card_warning'), max_length=20, null=True)
    insurance_company_warning = models.IntegerField(_('insurance_company_warning'), max_length=20, null=True)
    surface_confirm_warning = models.IntegerField(_('surface_confirm_warning'), max_length=20, null=True)
    inside_confirm_warning = models.IntegerField(_('inside_confirm_warning'), max_length=20, null=True)
    confirm_warning = models.IntegerField(_('confirm_warning'), max_length=20, null=True)
    damage_value_warning = models.IntegerField(_('damage_value_warning'), max_length=20, null=True)
    hand_in_case_warning = models.IntegerField(_('hand_in_case_warning'), max_length=20, null=True)
    damage_doc_warning = models.IntegerField(_('damage_doc_warning'), max_length=20, null=True)
    old_item_recycle_warning = models.IntegerField(_('old_item_recycle_warning'), max_length=20, null=True)
    receipt_warning = models.IntegerField(_('receipt_warning'), max_length=20, null=True)
    car_check_warning = models.IntegerField(_('car_check_warning'), max_length=20, null=True)
    case_can_close_warning = models.IntegerField(_('case_can_close_warning'), max_length=20, null=True)
    case_close_warning = models.IntegerField(_('case_close_warning'), max_length=20, null=True)

    class Meta:
        verbose_name = _('WarningSetting')
        verbose_name_plural = _('WarningSettings')

    def get_warning_setting(self, company):
        warning = WarningSetting.objects.get(company=company)
        return warning
