from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _
from insurance_log.apps.company.models import Company
from django.db.models import Q


class IlMember(AbstractUser):
    tel = models.CharField(_('tel'), max_length=20, null=True, blank=True)
    company = models.ForeignKey(Company, null=True, blank=True)
    priority = models.IntegerField(_('priority'), max_length=11,  null=True, blank=True)

    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'

    def __unicode__(self):
        return self.username

    def get_company_members(self, company, priority):
        members = IlMember.objects.filter(Q(company=company) & Q(priority <= priority))
        return members

    @classmethod
    def get_active_company_managers(self, company):
        members = IlMember.objects.filter(Q(company=company) & Q(priority=3) & Q(is_active=True))
        return members

    @classmethod
    def get_active_company_administrator(self, company):
        members = IlMember.objects.filter(Q(company=company) & Q(priority=1) & Q(is_active=True))
        return members

    def get_company_managers(self, company):
        members = IlMember.objects.filter(Q(company=company) & Q(priority=3))
        return members

    def get_member(self, user_id):
        members = IlMember.objects.get(id=user_id)
        return members