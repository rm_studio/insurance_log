from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from insurance_log.apps.ilmember.models import IlMember
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.hashers import make_password
from django.contrib.auth import authenticate, login, logout


def get_company_member(request):

    member = IlMember()
    members = member.get_company_members(request.user.company)

    return render_to_response('index.html', {"members": members}, context_instance=RequestContext(request))


def get_company_managers(request):

    member = IlMember()
    members = member.get_company_managers(request.user.company)

    return render_to_response('managers.html', {"members": members}, context_instance=RequestContext(request))


def get_active_company_manager(request):

    member = IlMember()
    members = member.get_active_company_managers(request.user.company)

    return render_to_response('managers.html', {"members": members}, context_instance=RequestContext(request))


def add_company_member(request):

    account = request.POST.get('tel')
    email = request.POST.get('account')+'@rmc.com'
    password = request.POST.get('password')
    try:
        user = IlMember.objects.create_user(account, email, password)
        user.first_name = request.POST.get('name')
        user.tel = request.POST.get('tel')
        user.company = request.user.company
        user.priority = request.POST.get('priority')
        user.is_active = True
        user.save()
    except:
        error = (_('tel has already existed'))

    return HttpResponseRedirect('/')


def add_company_manager(request):
    if request.method == 'POST':
        if not request.POST.get('name'):
            error = (_('please_input_manager_name'))
            return render_to_response('add_manager.html', {"error": error}, context_instance=RequestContext(request))
        if not request.POST.get('tel'):
            error = (_('please_input_tel'))
            return render_to_response('add_manager.html', {"error": error}, context_instance=RequestContext(request))
        tel = request.POST.get('tel')
        email = request.POST.get('tel')+'@rmc.com'
        try:
            user = IlMember.objects.create_user(tel, email, tel)
            user.first_name = request.POST.get('name')
            user.tel = request.POST.get('tel')
            user.company = request.user.company
            user.priority = 3
            user.is_active = True
            user.save()
        except:
            error = (_('tel has already existed'))
        return HttpResponseRedirect('/managers')
    return render_to_response('add_manager.html', {}, context_instance=RequestContext(request))



def edit_company_member(request, offset):
    member = IlMember()
    t_member = member.get_member(offset)
    if request.POST.get('name'):
        t_member.first_name = request.POST.get('name')
    if request.POST.get('tel'):
        t_member.tel = request.POST.get('tel')
    if request.POST.get('priority'):
        t_member.priority = request.POST.get('priority')
    t_member.save()
    return HttpResponseRedirect('/')


def edit_company_manager(request, offset):
    member = IlMember()
    t_member = member.get_member(offset)
    if request.POST.get('name'):
        t_member.first_name = request.POST.get('name')
    if request.POST.get('tel'):
        t_member.tel = request.POST.get('tel')
    t_member.save()
    return HttpResponseRedirect('/')


def active_company_member(request, offset):
    member = IlMember()
    t_member = member.get_member(offset)
    t_member.is_active = True
    t_member.save()
    return HttpResponseRedirect('/managers')


def inactive_company_member(request, offset):
    member = IlMember()
    t_member = member.get_member(offset)
    t_member.is_active = False
    t_member.save()
    return HttpResponseRedirect('/managers')


def change_password(request):
    me = request.user
    if request.method == 'POST':
        error = None
        if not request.POST.get('password_org'):
            error = (_('please_input_password_org'))
            return render_to_response('change_password.html', {"error": error}, context_instance=RequestContext(request))
        if not request.POST.get('password'):
            error = (_('please_input_password_new'))
            return render_to_response('change_password.html', {"error": error}, context_instance=RequestContext(request))
        if not request.POST.get('password_again'):
            error = (_('please_input_password_again'))
            return render_to_response('change_password.html', {"error": error}, context_instance=RequestContext(request))
        password_org = request.POST.get('password_org')
        password = request.POST.get('password')
        password_again = request.POST.get('password_again')
        print(me.password)
        print(password_org)
        print(make_password(password_org))
        print(request.user.username)
        user = authenticate(username=request.user.username, password=password_org)
        print(user)
        if user is None:
            error = (_('password_org_error'))
            return render_to_response('change_password.html', {"error": error}, context_instance=RequestContext(request))
        if password != password_again:
            error = (_('different_passwords'))
            return render_to_response('change_password.html', {"error": error}, context_instance=RequestContext(request))
        me.password = make_password(password)
        me.save()
        error = (_('password_changed'))
        logout(request)
        return render_to_response('login.html', {"error": error}, context_instance=RequestContext(request))

    return render_to_response('change_password.html', {"me": me}, context_instance=RequestContext(request))