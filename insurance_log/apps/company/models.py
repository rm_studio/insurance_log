from django.db import models
from insurance_log.core.atom.models import Atom
from django.utils.translation import ugettext_lazy as _
from ckeditor.fields import RichTextField
from django.db.models import Q


class Company(Atom):
    tel = models.CharField(_('tel'), max_length=150, default='', null=True, blank=True)
    address = models.CharField(_('address'), max_length=150, default='', null=True, blank=True)
    gps = models.CharField(_('gps'), max_length=50, default='', null=True, blank=True)
    open_time = RichTextField(_('open_time'), default='',  null=True, blank=True)
    type = models.CharField(_('type'), max_length=50, default='', null=True, blank=True)
    parent_company = models.ForeignKey('self', null=True, blank=True)
    available = models.BooleanField(_('available'), default=True)

    class Meta:
        verbose_name = _('Company')
        verbose_name_plural = _('Companies')

    def __unicode__(self):
        return self.name

    def get_available_insurance_companies(self, company):
        companies = Company.objects.filter(Q(parent_company=company) & Q(type="insurance") & Q(available=True))
        return companies

    @classmethod
    def get_insurance_companies(self, company):
        companies = Company.objects.filter(Q(parent_company=company) & Q(type="insurance"))
        return companies

    def get_company(self, company_id):
        company = Company.objects.get(id=company_id)
        return company
