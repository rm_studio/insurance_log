from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.template import loader, Context
from django.shortcuts import render_to_response
from django.template import RequestContext
from insurance_log.apps.company.models import Company
from django.utils.translation import ugettext_lazy as _



def get_active_insurance_companies(request):
    company = Company()
    companies = company.get_active_insurance_companies(request.user.company)
    return render_to_response('insurance_companies.html', {"companies": companies}, context_instance=RequestContext(request))


def get_insurance_companies(request):
    company = Company()
    companies = company.get_insurance_companies(request.user.company)
    return render_to_response('insurance_companies.html', {"companies": companies}, context_instance=RequestContext(request))


def add_insurance_company(request):
    if request.method == 'POST':
        if not request.POST.get('name'):
            error = (_('please_input_company_name'))
            return render_to_response('add_insurance_company.html', {"error": error}, context_instance=RequestContext(request))
        if not request.POST.get('tel'):
            error = (_('please_input_tel'))
            return render_to_response('add_insurance_company.html', {"error": error}, context_instance=RequestContext(request))
        company = Company()
        company.name = request.POST.get('name')
        company.tel = request.POST.get('tel')
        company.type = "insurance"
        company.parent_company = request.user.company
        company.available = True
        company.save()
        return HttpResponseRedirect('/insurance_companies')
    return render_to_response('add_insurance_company.html', {}, context_instance=RequestContext(request))


def edit_insurance_company(request, offset):
    company = Company()
    t_company = company.get_company(offset)
    if request.POST.get('name'):
        t_company.name = request.POST.get('name')
    if request.POST.get('tel'):
        t_company.tel = request.POST.get('tel')
    t_company.save()
    return HttpResponseRedirect('/')


def active_insurance_company(request, offset):
    company = Company()
    t_company = company.get_company(offset)
    t_company.available = True
    t_company.save()
    return HttpResponseRedirect('/insurance_companies')


def inactive_insurance_company(request, offset):
    company = Company()
    t_company = company.get_company(offset)
    t_company.available = False
    t_company.save()
    return HttpResponseRedirect('/insurance_companies')


