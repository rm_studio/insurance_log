from django.contrib import admin
from django.contrib.admin.models import LogEntry


class LogAdmin(admin.ModelAdmin):  
    list_display = ('id', 'object_repr', 'change_message', 'action_time', )  
    search_fields = ('object_repr', 'change_message',)  

    class Meta:  
        pass  

    class Admin:  
        pass


# Register your models here.
admin.site.register(LogEntry, LogAdmin)
