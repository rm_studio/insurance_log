from django.views.decorators.csrf import csrf_exempt
from django.template import loader, RequestContext
from django.shortcuts import render_to_response
from django.http import HttpResponse
from django.conf import settings
from .models import *
import time, base64


@csrf_exempt
def upload_test(request):
    return render_to_response('upload.html', context_instance=RequestContext(request))


@csrf_exempt
def check_existing(request):    
    return HttpResponse('0')
 

@csrf_exempt
def upload_image(request):

    snap_image = request.POST.get('snap_image')
    
    if snap_image:
        #if snap_image.split(',')[0] == 'data:image/png;base64':
        snap_image = request.POST.get('snap_image').split(',')[-1]

        file_title = 'capture' 
        file_name = time.strftime('%Y%m%d%H%M%S')
        file_ext = 'png'
        file_full_name = file_name+'.'+file_ext
        file_upload = open(os.path.join(UPLOAD_ROOT, file_full_name), 'w')
        file_data = base64.decodestring(snap_image)

    else:
        file_ext = str(request.FILES['Filedata'].name).split('.')[-1]
        file_title = str(request.FILES['Filedata'].name).split('.')[0]
        file_name = file_title+'.'+time.strftime('%Y%m%d%H%M%S')
        file_data = request.FILES['Filedata'].read()
        file_full_name = file_name+'.'+file_ext
        file_upload = open(os.path.join(UPLOAD_ROOT, file_full_name), 'w')

    file_upload.write(file_data)
    file_upload.close()

    file_path = UPLOAD_RELATE_ROOT + file_full_name 

    picture = GalleryUserPic(name=file_title, pic=file_path, links='#')
    picture.save()

    gallery_name = request.GET.get('name')
   
    try:
        gallery = GalleryUser.objects.get(name=gallery_name)
    except GalleryUser.DoesNotExist:
        gallery = GalleryUser(name=gallery_name)
        gallery.save()
    finally:
        gallery.galleryPic.add(picture)
        gallery.save()

    return HttpResponse("%s|%s" % (file_full_name, picture.pk) )


def delete_image(request):
    image_id = request.GET.get('pk')
    picture = GalleryUserPic.objects.get(pk=image_id)
    picture.delete()

    return HttpResponse(image_id)
