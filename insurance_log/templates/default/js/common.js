// format string.
String.prototype.format = function(args) {
    var result = this;
    if (arguments.length > 0) {    
        if (arguments.length == 1 && typeof (args) == "object") {
            for (var key in args) {
                if(args[key]!=undefined){
                    var reg = new RegExp("({" + key + "})", "g");
                    result = result.replace(reg, args[key]);
                }
            }
        }
        else {
            for (var i = 0; i < arguments.length; i++) {
                if (arguments[i] != undefined) {
                    //var reg = new RegExp("({[" + i + "]})", "g");//这个在索引大于9时会有问题，谢谢何以笙箫的指出
　　　　　　　　　　　　var reg= new RegExp("({)" + i + "(})", "g");
                    result = result.replace(reg, arguments[i]);
                }
            }
        }
    }
    return result;
};


// format time.
Date.prototype.format = function(format)
{
	/*
	 * * format="yyyy-MM-dd hh:mm:ss";
	 * */
	var o = {
		"M+" : this.getMonth() + 1,
		"d+" : this.getDate(),
		"h+" : this.getHours(),
		"m+" : this.getMinutes(),
		"s+" : this.getSeconds(),
		"q+" : Math.floor((this.getMonth() + 3) / 3),
		"S"  : this.getMilliseconds()
	}
	if (/(y+)/.test(format)) {
		format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	}
	for (var k in o) {
		if (new RegExp("(" + k + ")").test(format)) {
			format = format.replace(RegExp.$1, RegExp.$1.length == 1
				? o[k] : ("00" + o[k]).substr(("" + o[k]).length));
	}}
	return format;
}

// bind print for images.
$.fn.extend({
	printImage: function() {
			if( 'IMG' == $(this).prop("tagName") ) {
				if( !$('#print').length ) $('body').append('<iframe style="display: none;" id="print">');
				$('#print').contents().find("body").append( "<p align='center'><img/></p>" );
				$('#print').contents().find("body img").attr( "src", $(this).attr("src") ).css({ width: '100%' });	

				$('#print').contents()[0].execCommand('print');
			}
	},
});

