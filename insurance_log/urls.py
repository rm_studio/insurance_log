from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from insurance_log.apps.atom.views import *
from insurance_log.apps.progress.views import *
from insurance_log.apps.company.views import *
from insurance_log.apps.ilmember.views import *
from .core.log.views import *
from .core.gallery.views import *


urlpatterns = patterns('',
    url(r'^$', all_progresses),
    url(r'^ongoing_progresses', ongoing_progresses),
    url(r'^warning_progresses', warning_progresses),
    url(r'^detail/(\d+)$', detail),
    url(r'^search_progresses', search_progresses),

    url(r'^warning_setting', warning_setting_detail),
    url(r'^edit_warning_setting', edit_warning_setting),

    url(r'^managers', get_company_managers),
    url(r'^add_manager', add_company_manager),
    url(r'^active_member/(\d+)', active_company_member),
    url(r'^inactive_member/(\d+)', inactive_company_member),
    url(r'^insurance_companies', get_insurance_companies),
    url(r'^add_insurance_company', add_insurance_company),
    url(r'^active_insurance_company/(\d+)', active_insurance_company),
    url(r'^inactive_insurance_company/(\d+)', inactive_insurance_company),

    url(r'^change_password', change_password),

    url(r'^check_warning/$', check_warning),
    url(r'^get_warning_progresses_ajax$', get_warning_progresses_ajax),


    url(r'^login/', user_login),
    url(r'^logout/', user_logout),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^log-test/', log_test),
    url(r'^upload-test/', upload_test),
    url(r'^upload_image/', upload_image), 
    url(r'^delete_image/', delete_image), 
    url(r'^check_existing/', check_existing),
    url(r'^create$', create),
    url(r'^progress/(\d+)$', step),
    url(r'^delete-progress/?', delete_progress), 
    #url(r'^grappelli/', include('grappelli.urls')),
)

# Append
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)    
urlpatterns += staticfiles_urlpatterns()
